import re
from .base import GeometryReader

class TSReader(GeometryReader):

    def _read(self, path):
        vertices, faces = [], []

        with open(path) as f:
            for line in f.readlines():
                if line.startswith("VRTX"):
                    vertex = re.split('\s+', line)[1:5]
                    vertices.append(tuple(float(i) for i in vertex[1:4]))
                elif line.startswith("PVRTX"):
                    vertex = re.split('\s+', line)[1:5]
                    vertices.append(tuple(float(i) for i in vertex[1:4]))
                elif line.startswith("TRGL"):
                    faces.append(tuple(int(i)
                                 for i in re.split('\s+', line)[1:4]))
                else:
                    pass

        return vertices, faces
