from .base import GeometryReader


class OBJReader(GeometryReader):

    def _read(self, path):
        vertices, faces = [], []

        with open(path) as f:
            for line in f.readlines():
                if line.startswith("v"):
                    v = line.rstrip().split(" ")[1:]
                    verts = [float(item) for item in v]
                    vertices.append(tuple(verts[i] + self.transform[i] for i in range(3)))
                elif line.startswith("f"):
                    f = line.rstrip().split(" ")[1:]
                    faces.append(tuple(int(abc) for abc in f))
                else:
                    pass

        return vertices, faces
