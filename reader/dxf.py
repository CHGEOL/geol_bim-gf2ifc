import ezdxf
from .base import GeometryReader


class DXFReader(GeometryReader):

    def _read(self, path):
        dxf = ezdxf.readfile(path)
        model = dxf.modelspace()
        vertices, faces = [], []
        i = 0

        for entity in model:
            i += 1
            faces.append((i * 3 - 2, i * 3 - 1, i * 3 - 0))
            for j in range(3):
                vertices.append(tuple(entity[j]))
        return vertices, faces
