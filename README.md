# GEOL_BIM-gf2ifc

Transformation von Grenzflächen- und Körpergeometrien (geologic features) in das IFC-Format.<br/><br/>

 
# 1. Übersicht
Mit diesem Python-Skript werden Grenzflächen- und Körperdaten nach IFC transformiert. Dabei gelten folgende Rahmenbedingungen und Transformationsregeln:

* Quellformat: Die Daten sind im Format .xlsx bereitzustellen, wobei die einzelnen Grenzflächen oder Körper in separaten Dateien beschreieben sind. Im .xlsx wird jeweils auf diese Dateien verwiesen.
  * Ein .xlsx Blatt enthält Metadaten
  * Ein .xlsx Blatt enthält die Auflistung der Grenzflächen und Körper -> "GeologicFeatures":
    * Jedes Feature verweist auf eine Datei, welche die Geometrie beschreibt. Zu jeder Grenzfläche oder zu jedem Körper können beliebige Informationen angehängt werden (Porosität, Feuchtraumgewicht, etc.). 
    * Mögliche Formate für die Geometriedateien sind: dxf, obj, stl und ts.
  * Für jede Grenzfläche oder für jeden Körper können beliebige Eigenschaften definiert werden (als frei definierbare Spalten der xlsx-Datei). Die Transformation dieser Eigenschaften ins Datenmodell von IFC kann über die Konfiguration ausgelöst werden.

  * --> Die Struktur der xlsx-Datei werden weiter unten im Detail beschrieben (Abschnitt Konfiguration).


* Zielformat: Die Grenzflächen und Körper werden nach IFC transformiert.
  * IFC-Version: 4x1
  * Aus jedem Feature wird ein einzelner IfcBuildingElementProxy erzeugt.
  * Die Grenzflächen und Körper werden in der Raumstruktur direkt der IfcSite angefügt.
  * Die Art der Georeferenzierung in IFC kann gewählt werden. Es wird dazu das Konzept der "Level of Georeferencing" (LoGeoRef) verwendet und die Level 30, 40, 50 unterstützt.

## 1.1. Installation: 
 1. Download Anaconda
 2. Download Gitlab Repository
 3. Erstellen der Entwicklungssumgebung aus der yml-Datei im Installationsordner: `conda env create -f py37gf2ifc.yml`
 4. Download IfcOpenShell für Python 3.7 (http://ifcopenshell.org/python), lokal ablegen: _C:\Anaconda3\envs\py37gf2ifc\Lib\site-packages_
 5. Aktivieren der neuen Umgebung in der Anaconda Prompt mit dem Befehl: `conda activate py37gf2ifc`

## 1.2. Benutzung:

 - Exportieren der Grenzflächen- und Körpergeometrien aus der 3D-Software als .xlsx-Datei plus zusätzlichen Geometriedateien und diese ablegen in den Ordner "data/input"
 - Anpassung des config.yml **NUR** an den kommentierten respektive den weiter unten im Readme beschriebenen Stellen anpassen
 - Verwendung der Datei "gf2ifc.py", für die Erstellung der IFC-Datei zur Darstellung der Grenzflächen und Körper 

 <br/>
  
 ## 1.3. Konfiguration - Verwendung .config FIle:
Der Transferprozess kann über eine Konfiguration gesteuert werden. Die Konfigurationsdatei kann an verschiedenen Stellen für die eigene Verwendung angepasst werden. Diese Variablen sind im Abschnitt `Beschreibung der Variablen aus der config.yml Datei` beschrieben.

Mit Hilfe der Variablen im Abschnitt "ColumnsForGeologicalFeatureDefinition" des config.yml Files werden die Spaltennamen aus dem exportierten .xlsx File auf die Struktur des gf2ifc.py Skripts angepasst. Das genaue Vorgehen wird in den folgenden zwei Abschnitten erklärt:
  
### 1.3.1. **ColumnsForGeologicalFeatureDefinition**: 
In diesem Abschnitt der Konfigurationsdatei werden den vorgegbenen Variablennamen die entsprechenden Spaltennamen in der .xlsx Datei zugeordnet. Die Variablennamen auf der linken Seite dürfen **NICHT** verändert werden. Auf der rechten Seite stehen die Spaltennamen aus dem vom Benutzer erstellten .xlsx File.
Die Variablen in diesem Abschnitt sind unabdingbar. Sie dürfen nicht ergänzt werden und es darf keine dieser Variablen weggelassen werden, da sie essenziell für die Konstruktion der einzelnen Features sind.
 <br/>

### 1.3.2. **PropertySetDefinition:**
In diesem Abschnitt der Konfigurationsdatei werden die Namen der IFC Properties definiert und in IFC Property Sets gruppiert. Gleichzeitig wird für jedes IFC Property angegeben, welchem Spaltennamen es im Blatt Interval in der .xlsx Datei entspricht.
Die Namen von Property Sets stehen auf der ersten Einrückungsebene unter PropertySetDefinition und enden mit einem Doppelpunkt ohne, dass rechts davon etwas steht.
Die IFC Property Namen stehen auf der zweiten Einrückungsebene, also eine Einrückung tiefer als das beinhaltende Property Set. Der IFC Property Name ist gefolgt von einem Doppelpunkt und dem Namen der Spalte im .xlsx Blatt Interval.
<br/>
Die Definitionen der Property Sets und der Properties können in der Konfigurationsdatei frei formuliert werden. Für die Schreibweise der Namen für die IFC Property Sets sowie der Properties selber sollte jedoch die Kamelschreibweise (mit Binnenmajuskel) gewählt werden. Ausserdem sind englische Begriffe für eine bessere sprachregion-übergriffende Handhabung vorzuziehen. z.B.: LayerDescription 

Ganzes Beispiel für ein IFC Property:<br/>
LayerDescription: LAYERDESC 
<br/>
In diesem Beispiel heisst die .xlsx Spalte also lediglich "LAYERDESC". Das entsprechende IFC Property jedoch "LayerDescription"
<br/> 

# 2. Beschreibung der Variablen aus der config.yml Datei:
Für die Abschnitte BasicProjectInformation und ColumnsForGeologicalFeatureDefinition folgt eine Auflistung der Variablennamen und eine Beschreibung ihrer Funktion oder Bedeutung.
| Variable | Beschreibung |
| ----------- | ----------- |
| **BasicProjectInformation** |  |
| InputFileName| Name der vorgegebenen .xlsx Datei |
| Creator | Initialen / Name des Benutzers |
| Organization | Name der Organisation |
| OutputFilName | Name der resultierenden .ifc Datei |
| ProjectDescription | Projektbeschreibung für IfcProject |
| ProjectName | Projektname  |
| ProjectOrigin | Projektnullpunkt, Angabe der x-, y-, z-Koordinaten in LV95 Koordinaten |
| Rotation | Projektrotation zum Weltkoordinatensystem für die Georeferenzierung (Drehung VON echtem Norden zur positiven y-Achse in Grad, gegen den Uhrzeigersinn (rechtshändiges kartesisches Koordinatensystem). Rotationszentrum == Ursprung. Drehung des zugrundeliegenden Projektkoordinatensystems, relativ zum wahren Norden (geografische Nordrichtung)) |
| SchemaVersion | IFC Version (IFC2x3, IFC4 oder IFC4x1) |
| LoGeoRef | Level of Georeferencing (= Georeferenzierungsebene). Angabe der Level 30, 40 oder 50. Weitere Informationen unter: https://jgcc.geoprevi.ro/docs/2019/10/jgcc_2019_no10_3.pdf |
| IfcSiteName | Name des Grundstücks |
| ----------- | ----------- |
| **ColumnsForGeologicalFeatureDefinition** |  |
| GeolFeatID | Eindeutige ID der Grenzfläche oder des Körpers |
| GeolFeatFile | Name der Geometriedatei |
| GeolFeatColorR, GeolFeatColorG, GeolFeatColorB | Farbwerte rot, gelb, blau für die Geometrie im Bereich 0 bis 256, wobei 0 keine und 256 die volle Farbintensität bedeutet |

