# IDIBAU, FHNW
# DATE:    2021-03-20 / LS
# NAME:    log_config
# PROJECT: geol_bim
# 
# ----------------------------------------------------


def log_config(logfile):
    config = dict(
        version=1,
        formatters={
            'file': {'format': '%(asctime)s - %(message)s',
                     'datefmt': '%Y-%m-%d %H:%M:%S'},
            'console': {'format': '%(asctime)s - %(name)s - %(levelname)s - %(message)s'}},
        handlers={
            'file': {'class': 'logging.FileHandler',
                     'filename': logfile,
                     'formatter': 'file',
                     'level': 'INFO'},
            'console': {'class': 'logging.StreamHandler',
                        'formatter': 'console',
                        'level': 'DEBUG'}
                },
        loggers={
            'geol_bim': {'level': 'DEBUG',
                    'handlers': ['file', 'console']}
                }
    )

    return config

