# ====================================================
# Read units and structures and transform them to ifc
# ====================================================

# Source code base: http://academy.ifcopenshell.org/creating-a-simple-wall-with-property-set-and-quantity-information/
# Utils and other functions also from OGN project

import os
from os import path
import geol_bim.ifc_utils as ifc # Import Methods for writing IFC
import time
import pandas as pd 
import yaml
import logging
import sys
from logging.config import dictConfig
from geol_bim.log_config import * # Logging configuration geol_bim
from reader.dxf import DXFReader
from reader.obj import OBJReader
from reader.stl import STLReader
from reader.ts import TSReader

BPI_CFG_KEY = 'BasicProjectInformation'
PSD_CFG_KEY = 'PropertySetDefinition'


# Setup logging
if not os.path.exists('data/output'):
    os.makedirs('data/output')
timestamp = time.time()
timestring = time.strftime('%Y%m%dT%H%M%S', time.gmtime(timestamp))
loggingConfiguration = log_config(path.join('data', 'output', f'{timestring}.log'))
dictConfig(loggingConfiguration)  # set config
logger = logging.getLogger('geol_bim')  # start logger


# Create and validate units and structures info from config file data
class GeolFeatToExtNames():

    def __init__(self, cfg):

        def grantKeyPresent(cfg, key, message, noAttr = False):
            if not key in cfg:
                sys.exit(message.format(key))
            if not noAttr:
                setattr(self, key, cfg[key])
        
        # Validate config keys
        DIS_CFG_KEY = 'ColumnsForGeologicalFeatureDefinition'
        grantKeyPresent(cfg, DIS_CFG_KEY, 'Die Konfigurationsdatei enthält keinen {} Abschnitt.', True)
        sgi = cfg[DIS_CFG_KEY]
        secMsg = 'Der Abschnitt {} in der Konfigurationsdatei enthält keinen {{}} Schlüssel.'.format(DIS_CFG_KEY)
        grantKeyPresent(sgi, 'GeolFeatID', secMsg)
        grantKeyPresent(sgi, 'GeolFeatFile', secMsg)
        grantKeyPresent(sgi, 'GeolFeatColorR', secMsg)
        grantKeyPresent(sgi, 'GeolFeatColorG', secMsg)
        grantKeyPresent(sgi, 'GeolFeatColorB', secMsg)
        # TODO optional columns
        # optional grantKeyPresent(sgi, 'GeolFeatLocator', secMsg)

def colorFromXlsx(val, rKey, gKey, bKey):
    def readOne(key):
        try:
            n = (abs(int(val[key])) % 256) / 256
        except:
            n = 0
        return n
    return [readOne(rKey),readOne(gKey),readOne(bKey)]

def createIfcProperties(cfgPsd, ifcFile, ownerHistory, segmentSpaces, rowDic, dataSrcLabel):
    for key, values in cfgPsd.items():
        propertySingleValues = [] # list with SingleValues, values fromexample 59=IfcPropertySingleValue('volume_factor','volume_factor',IfcText('80.39329512'),$)
        for label, value in values.items():
            valueStr = '{}'.format(value)
            segValue = rowDic[valueStr]
            propertySingleValueWriter = ifcFile.createIfcPropertySingleValue(label, label, ifcFile.create_entity('IfcText', str(segValue)), None)
            propertySingleValues.append(propertySingleValueWriter)
        propertySet = ifcFile.createIfcPropertySet(ifc.create_guid(), ownerHistory, '{}'.format(key), None, propertySingleValues)
        ifcFile.createIfcRelDefinesByProperties(ifc.create_guid(), ownerHistory, None, None, segmentSpaces, propertySet)

def writeGeometryToProxy(
        cfgPsd, ifcFile, context, ownerHistory, proxyPlacement, site, 
        cartesianPointList, triangulatedFaceSet, filename, rowDic, dataSrcLabel, rgb):
    
    cords = ifcFile.createIfcCartesianPointList3d(cartesianPointList)
    faces = ifcFile.createIfcTriangulatedFaceSet(cords, None, None, triangulatedFaceSet, None)

    # Colour geometries
    colour = ifcFile.createIfcColourRgb(None,rgb[0],rgb[1],rgb[2])
    styleShading = ifcFile.createIfcSurfaceStyleShading(colour, None)
    surfaceStyle = ifcFile.createIfcSurfaceStyle(None,"BOTH",(styleShading,))
    styledItem = ifcFile.createIfcStyledItem(faces,(surfaceStyle,),None)

    bodyRepresentation = ifcFile.createIfcShapeRepresentation(context, "Body", "SweptSolid", [faces])
    productShape = ifcFile.createIfcProductDefinitionShape(None, None, [bodyRepresentation])
    proxy = ifcFile.createIfcBuildingElementProxy(ifc.create_guid(), ownerHistory, filename, None, None, proxyPlacement, productShape, None)

    createIfcProperties(cfgPsd, ifcFile, ownerHistory, [proxy], rowDic, dataSrcLabel)
    
    ifcFile.createIfcRelContainedInSpatialStructure(ifc.create_guid(), ownerHistory, "Building Storey Container", None, [proxy], site)


def main():

    logger.info('Converting units and structures data to IFC.')
    startTime = time.time()
                
    try:
        # create variables from the config file
        dir = os.path.dirname(__file__)
        cfgFilePath = os.path.join(dir, 'gf2ifc_config.yml')
        cfg = yaml.safe_load(open(cfgFilePath,'r', encoding='utf-8'))

        cfgDic = GeolFeatToExtNames(cfg)

        cfgBpi = cfg[BPI_CFG_KEY]

        # create IFC-header
        geoRef = None
        if cfgBpi['LoGeoRef'] == 30:
            geoRef = 'IfcMapConversion' 
        elif cfgBpi['LoGeoRef'] == 50:
            geoRef = 'IfcGeometricRepresentationContext' 
        else: 
            sys.exit('invalid level of georeferencing')

        template = ifc.create_template(
            cfgBpi['OutputFileName'], cfgBpi['Creator'], cfgBpi['ProjectName'], cfgBpi['Organization'], 
            cfgBpi['SchemaVersion'], 
            minimal = False, 
            file_descr=cfgBpi['ProjectDescription'],  
            project_descr=cfgBpi['ProjectDescription'], 
            #authorization=cfgBpi['Authorization'], 
            origin = list(cfgBpi['ProjectOrigin'].values()), 
            rotation=cfgBpi['Rotation'], 
            georeferencing = geoRef
        )

        # write template to temp-file
        unusedTempHandle, tplFilename = ifc.template2tempfile(template)
        ifcFile, project, ownerHistory, context = ifc.open_ifc(tplFilename)

        # create IfcSite: Site base point (IfcSite.ObjectPlacement) at the project origin (0, 0, 0)
        site, sitePlacement = ifc.create_ifcsite_with_survey_point(
            ifcFile, project, ownerHistory, context, 
            origin = list(cfgBpi['ProjectOrigin'].values()),
            rotation=cfgBpi['Rotation'], 
            georef=geoRef, descr=cfgBpi['IfcSiteName']
        )

        # if cfgBpi[SGT_CFG_KEY] == SGT_V_SEC_SOLID_HOR:
        #     alignmentPlacement = ifc.create_ifclocalplacement(ifcFile, relative_to=sitePlacement)

        # Importing .xls File and naming the column names   
        geolFeatFileSheet = pd.read_excel(os.path.join(dir,'data','input', cfgBpi['InputFileName']),sheet_name='GeologicFeatures', skiprows=[0,2])

        idKey = cfgDic.GeolFeatID
        fileKey = cfgDic.GeolFeatFile
        # TODO locator

        # Check all geological features files/locators exist and are valid
        for i, val in geolFeatFileSheet.iterrows():
            # id = float(val[idKey])
            fileName = str(val[fileKey])
            gPath = os.path.join(dir,'data','input', fileName)
            if not os.path.isfile(gPath):
                sys.exit('Die in {} angegebene Datei {} ist nicht vorhanden.'.format(cfgBpi['InputFileName'], fileName))

        # Read each feature file
        for i, val in geolFeatFileSheet.iterrows():
            id = float(val[idKey])
            fileName = str(val[fileKey])
            rgb = colorFromXlsx(val, cfgDic.GeolFeatColorR, cfgDic.GeolFeatColorG, cfgDic.GeolFeatColorB)

            # Derive reader type from file extension.
            lfn = fileName.lower()
            # It seems, that there is no need to pass optional transform to readers.
            if lfn.endswith('.stl'):
                rdr = STLReader()
            elif lfn.endswith('.dxf'):
                rdr = DXFReader()
            elif lfn.endswith('.obj'):
                rdr = OBJReader()
            elif lfn.endswith('.ts'):
                rdr = TSReader()

            gPath = os.path.join(dir,'data','input', fileName)
            vertices, faces = rdr.read(gPath) # cartesianPointList, triangulatedFaceSet

            placement = ifc.create_ifclocalplacement(ifcFile, relative_to=sitePlacement)
            writeGeometryToProxy(
                cfg[PSD_CFG_KEY], ifcFile, context, ownerHistory, placement, site, 
                vertices, faces, fileName, val, 'row {}'.format(i), rgb
            )
                
        # containerSpace = ifcFile.createIfcRelAggregates(ifc.create_guid(), ownerHistory, 'Space Container', None, site, ifcSpaces)

        oPath = os.path.join(dir,'data','output', cfgBpi['OutputFileName'])
        ifcFile.write(oPath)
        logger.info('Successfully done!')

        # time duration to calculate segments
        def timecalc(calculatedSeconds):
            secondsInDay = 60 * 60 * 24
            secondsInHour = 60 * 60
            secondsInMinute = 60
            days = calculatedSeconds // secondsInDay
            hours = (calculatedSeconds - (days * secondsInDay)) // secondsInHour
            minutes = (calculatedSeconds - (days * secondsInDay) - (hours * secondsInHour)) // secondsInMinute
            return(minutes)
        calcSecs = time.time() - startTime
        logger.info('The script finished in {} minutes'.format(timecalc(calcSecs)))
    except:
        logger.exception('Got exception on main handler')
        raise

main()
